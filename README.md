# chef-fun

*Code your C++ with that special functional taste*

[![pipeline status](https://gitlab.com/libchef/chef-fun/badges/master/pipeline.svg)](https://gitlab.com/libchef/chef-fun/commits/master)
[![coverage report](https://gitlab.com/libchef/chef-fun/badges/master/coverage.svg)](https://gitlab.com/libchef/chef-fun/commits/master)

## Description

Chef-fun is a platform agnostic C++ library that provides common types for
functional programming. The goals is to provide C++ programmer with the same
tools that are available in other functional oriented languages such as Scala.

The library is header only, and does not require any external dependency. It
is written in C++20, and has been tested with gcc 9.4.3. Since no gcc specific
extensions or features have been used the library should be usable also with
other compilers.

Chef-fun is part of the Chef library, but can be used alone since there are no
dependencies from the other modules.

## Getting started

The library is header only, so you may download the files and place
them in your include path.

If you are using cmake, you may use the following code to make ChefFun available
to your project:

```cmake
cmake_minimum_required(VERSION 3.14)

#...

include( FetchContent )

FetchContent_Declare(
  chef-fun
  GIT_REPOSITORY https://gitlab.com/libchef/chef-fun.git
  GIT_TAG        1.0.1
)

FetchContent_MakeAvailable( chef-fun )

target_include_directories(
  YourTarget
  PUBLIC
  ${chef-fun_SOURCE_DIR}/include
  # other include paths
)
```

## Support
The code is provided as is, there is no granted support.

You may contact me via email, but I cannot guarantee any answer.

## Roadmap
Option and Either are quite stable. Next I would like to implement
Future<>, and, even better IO effect.

## Contributing
Users are welcome, contributions even more. Forking and proposing MR are the
preferred way to make contributions. Feel free to contact the author.

## Authors and acknowledgment
see [AUTHORS] file.

## License
[Apache License v2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Project status
The project is young, but the code you find here has been used quite 
extensively in industrial projects. The interface is stable, but the
implementation may be improved.

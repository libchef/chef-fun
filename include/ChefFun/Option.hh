/**
 * @addtogroup ChefFun
 * @{
 * @file ChefFun/Option.hh
 * @author Massimiliano Pagani
 * 
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined( CHEFFUN_OPTION_HH )
#define CHEFFUN_OPTION_HH

#include <utility>
#include <stdexcept>
#include <functional>
#include <type_traits>


#if !defined( __cpp_exceptions )
#  include <ChefDebug/assert.hh>
#endif

namespace ChefFun
{
    /**
     * Optional value template.
     * 
     * This is the elegant way to provide optional values. Traditionally C/C++
     * provides this semantic via null pointers. A null pointer means that a
     * value is not present.
     * 
     * This is convenient, but has a number of drawbacks. For example you always
     * need to test the pointer against nullptr to determine whether it is good
     * or not.
     * 
     * Also not everything is convenient to be stored/passed around as a 
     * pointer. For example an int.
     *
     * @note you can specialize the template so to avoid the overhead of the
     *       implementation. E.g. consider you want an option for pointers and
     *       you are happy to use the `nullptr` as the marker for no value.
     *       Then you can avoid having a boolean to state the data validity as
     *       per the current implementation.
     */
    
    template<typename T>
    class Option
    {
        public:
            using BaseType = T;

            /**
             * According to standard Option, you need Some and None.
             * 
             * @param value the value to place in the option.
             * @return the option.
             * @{
             */
            static Option<T> Some( T const& value ) noexcept( noexcept( T(value)));
            static Option<T> Some( T&& value ) noexcept;
            template<typename ... Args>
            static Option<T> Some( Args&&... params ) noexcept( noexcept( T(params...) ) );
            static Option<T> None() noexcept;
            /** @} */

            explicit Option( T&& t ) noexcept;

            template<typename U>
            explicit Option( Option<U> const& copy ) noexcept(noexcept(T(std::declval<U>())));

            Option( Option<T> const& copy ) noexcept( std::is_nothrow_copy_constructible_v<T> );

            Option( Option<T>&& from) noexcept;

            ~Option();
            
            /**
             * Checks for option validity.
             * 
             * @retval true if the value is valid/present
             * @retval false if the value is not valid nor present.
             */
            [[deprecated("use isDefined/isEmpty instead")]]
            [[nodiscard]] bool isValid() const noexcept;

            /**
             * Determines if the option has a value.
             *
             * @return true if the option is defined (i.e. contains a value)
             * @see isEmpty()
             */
            [[nodiscard]] bool isDefined() const noexcept;

            /**
             * Determines if the option has no value.
             *
             * @return true if the option has no value.
             * @see isDefined()
             */
            [[nodiscard]] bool isEmpty() const noexcept;

            /**
             * Assignment to another option. Destructors and/or assignment 
             * operators are properly called.
             * 
             * @param option the option to assign.
             * @return A reference to the option itself.
             */
            Option<T>& operator=( Option<T> const& option ) noexcept( std::is_nothrow_copy_constructible_v<T> );
            
            /**
             * Assignment move operator. Content of the rhs option is moved
             * into this option. The rhs option becomes None.
             *
             * @param option the rhs option.
             * @return an option containing the rhs value (if any, or None).
             */
            Option<T>& operator=( Option<T>&& option ) noexcept;

            /**
             * Move the value out of the option. If the option is Some. When
             * the option is None then it is an error and an exception is
             * thrown.
             *
             * @return the object. This is the only copy available.
             * @throws std::invalid_argument if the option is not valid.
             */
            [[deprecated("modifies the Option")]]
            T move();

            /**
             * Retrieves the object contained in the Option. If the option is
             * not valid, then an exception is thrown.
             * 
             * @return the object contained in the option if defined.
             * @throws std::invalid_argument if the option is not valid.
             */
            T const& get() const;

            /**
             * If the option is defined then the value is returned, otherwise a
             * default value is returned. This function may throw only if copy
             * constructor for T throws.
             * 
             * @param defaultValue the default value that will be returned if
             *                     this option is not defined.
             * @return the value of the option, if defined, or defaultValue, if
             *         not defined.
             */
            T getOrElse( T const& defaultValue ) const noexcept;

            /**
             * If the option is defined then the option is returned, otherwise
             * the option returned by an invocation of function f, is returned.
             *
             * @param f a callable object that accept no argument and returns an
             *          Option<T>.
             * @return if this->isEmpty() -> f(), otherwise *this.
             */
            template<typename F>
            [[nodiscard]] Option<T> orElse( F&& f ) const noexcept( noexcept( f() ) );

            /**
             * Retrieves a pointer to the object contained in the Option, or a
             * nullptr if the option is empty.
             * 
             * @return a pointer to the contained object if isDefined() yields
             *         true, or nullptr when isDefined() yields false.
             * @{
             */
            [[nodiscard]] T const* getPtrIfValid() const noexcept;
            [[nodiscard]] T* getPtrIfValid() noexcept;
            /** @} */

            /**
             * Map and flatMap operations.
             * @param f a function that transform a T into an U (or a T in an
             *          Option<U>).
             * @return an Option<U> containing the converted T, or None if the
             *         option is not defined.
             */
            template<typename F>
            [[nodiscard]] auto map( F f ) const noexcept(
                noexcept(
                    f( std::declval<T>() )
                )
            ) -> Option<std::invoke_result_t<F,T>>;

            /**
             * FlatMaps the option.
             *
             * @tparam F a function of type T -> Option<T1>
             * @param f a function of type F
             * @retval f(get()).get() if the option has a value
             * @retval Option<T1>::None() if the option has no value.
             */
            template<typename F>
            [[nodiscard]] auto flatMap( F f ) const noexcept(
                noexcept(
                    f(std::declval<T>() )
                )
            ) -> std::invoke_result_t<F,T>;

            /**
             * Applies the given function to the value of the Option if the
             * Option isDefined, otherwise the function is not invoked.
             *
             * @tparam F the type of a function T -> void
             * @param f a function of type F.
             * @{
             */
            template<typename F>
            void
            foreach( F const&  f ) const noexcept( std::is_nothrow_invocable_v<F,T> );

            template<typename F>
            void
            foreach( F const& f ) noexcept( std::is_nothrow_invocable_v<F,T> );
            /** @} */

            /**
             * The OptionIterator allows you to handle the Option as a container
             * of zero or one element and use standard algorithms and for
             * keyword.
             */
            class OptionIterator : public std::input_iterator_tag
            {
                public:
                    explicit OptionIterator( Option<T> const& option ) noexcept;
                    OptionIterator() noexcept;
                    OptionIterator operator++() noexcept;
                    T operator*() const noexcept;
                    bool operator==( OptionIterator const& other ) const noexcept;
                    bool operator!=( OptionIterator const& other ) const noexcept;
                private:
                    Option<T> const* m_item;
            };

            /**
             * Option can be considered as a list of at most one element. As
             * such it can be used as a container and in C++ all containers have
             * a begin() and end() methods pair to let algorithm use them.
             *
             * @return an iterator either to the beginnning or to the end of the
             *         option. For None options begin() == end(). For Some
             *         options *begin() is equivalent to get().
             * @{
             */
            [[nodiscard]] OptionIterator begin() const noexcept;
            [[nodiscard]] OptionIterator end() const noexcept;
            ///@}

            /**
             * Squashes Option<Option<X>> into Option<X>.
             *
             * @note it is an error to apply this method on Option<T> where T is
             *       not an Option<U>.
             * @note it may be counterintuitive that this method returns T, but
             *       since this is an Option<Option<U>>, type of T is Option<U>.
             * @return  The inner Option or None.
             */
            [[nodiscard]] T flatten() const noexcept;


            /**
             * @{
             * Poor men pattern matching. In other languages you can perform
             * pattern matching to avoid if(isValid)... else.
             *
             * Avoiding if is usually a good way to get rid of possible bugs and
             * makes the code more readable.
             *
             * Two template methods matchSome() and matchNone are provided. They
             * are expected to be used as in the following example.
             *
             * @code
             *   Option<T> op = ...;
             *   auto result = op
             *      .matchSome( []( T&& t ){ return f(t); } )
             *      .matchNone( [](){ return g(); } );
             * @endcode
             *
             * Where f is invoked only if the option is valid, and g is invoked
             * only when the option is not valid.
             *
             * Note that f and g must return the same type.
             *
             * The code in the example is equivalent to:
             *
             * @code
             *   Option<T> op = ...;
             *   U result{};
             *   if( op.isDefined() )
             *   {
             *      result = f( op.get() );
             *   }
             *   else
             *   {
             *      return = g();
             *   }
             * @endcode
             *
             * @tparam F the type of the function to invoke for match. In the
             *           matchSome the value of the option is passed to the F,
             *           while for matchNone no argument is provided.
             *
             *           Both functions must return the same type.
             * @param f the function to apply (of type F)
             * @return matchSome() returns a new option with the type of F
             *         return type. While matchNone() return an object of the
             *         same type of the return type of the applied function.
             */
            template<typename F>
            Option<std::invoke_result_t<F,T>>
            matchSome( F f ) const noexcept( noexcept( f(std::declval<T>() )));

            template<typename F>
            std::invoke_result_t<F>
            matchNone( F f ) const noexcept( noexcept( f() ) );
            /** @} */

            /**
             * Filters the option. If the option is valid and the predicate is
             * true, the option is returned. Otherwise None is returned.
             *
             * @tparam F predicate type, must be a function of type T -> bool
             * @param f the predicate
             * @return *this if the predicate is true, None() otherwise.
             */
            template<typename F>
            Option<T> filter( F f ) const noexcept( noexcept( f(std::declval<T>()) ) );

        private:
            typename std::aligned_storage_t<sizeof(T), alignof(T)> m_data;
            bool m_isValid;

            /**
             * Constructs an Option. By default it is not valid.
             */
            Option() noexcept;

            /**
             * Construct an option with a value. This constructor yields a 
             * valid option.
             * 
             * @param t the value of the option.
             */
            explicit Option( T const& t ) noexcept( noexcept( T(t) ) );

            T& dataRef() noexcept
            {
                return *reinterpret_cast<T*>( &m_data );
            }
            
            T const& dataRef() const noexcept
            {
                return *reinterpret_cast<T const*>( &m_data );
            }
            
    };

    template<typename T>
    Option<T> Some( T const& value );

    template<typename T>
    Option<T> Some( T && value );

    template<typename T>
    Option<T> None();

    template<typename T, typename U> bool
    operator==( Option<T> const& lhs, Option<U> const& rhs ) noexcept;

    template<typename T, typename U> bool
    operator!=( Option<T> const& lhs, Option<U> const& rhs ) noexcept;

    /**
     * Converts a sequence of options of types T1, T2, ... into an option of
     * a type T that has a constructor accepting T1, T2, ....
     *
     * If one or more options are empty, then the resulting option will be
     * empty as well.
     *
     * Example:
     *
     * @code
     *   struct Foo {
     *       int n;
     *       std::string s;
     *   };
     *
     *   Option<Foo> a = sequence<Foo>( Option::Some(1), Option::Some("hello"));
     *   // a is defined with value Foo{ 1, "hello" }
     *   Option<Foo> b = sequence<Foo>( Option::Some(1), Option::None() );
     *   // b is empty, since the string argument option is None.
     * @endcode
     *
     * The sequence function, is similar to the traverse.
     *
     * @tparam T the type in the resulting option.
     * @tparam O the types of each option argument.
     * @param options the option of arguments.
     * @return the option of a type constructed with the option arguments.
     */
    template<typename T, typename ...O>
    Option<T>
    sequence( O... options );

    /**
     * Converts a vector of Option<T> in an Option of vector<T>. This is
     * similar to the traverse invoked with the identity function.
     *
     * @tparam T the inner type
     * @param options the array to convert
     * @retval Some(vector<T>) if all the options in the argument vector are
     *                         defined.
     * @retval None() if one or more option in the argument vector are empty.
     */
    template<typename T>
    Option<std::vector<T>>
    sequence( std::vector<Option<T>> const& options );


#include <ChefFun/Option_void.tcc>

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::: Implementation :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename T> inline
    Option<T>::Option() noexcept : m_isValid{ false }
    {
    }

    template<typename T> inline
    Option<T>::Option( T const& t ) noexcept( noexcept( T(t) )) : m_isValid{ true }
    {
        new( &m_data ) T{ t };
    }

    template<typename T> inline
    Option<T>::Option( Option<T> const& copy ) noexcept(std::is_nothrow_copy_constructible_v<T>)
              : m_isValid{ copy.isDefined() }
    {
        if( m_isValid )
        {
            new( &m_data ) T{ copy.get() };
        }
    }

    template<typename T> template<typename U> inline
    Option<T>::Option( Option<U> const& copy ) noexcept(noexcept(T(std::declval<U>())))
              : m_isValid{ copy.isDefined() }
    {
        if( m_isValid )
        {
            new( &m_data ) T{ copy.get() };
        }
    }

    template<typename T> inline
    Option<T>::Option( Option<T>&& from) noexcept
        : m_isValid{from.isDefined() }
    {
        if( m_isValid )
        {
            new( &m_data ) T( std::move(from.dataRef() ) );
            from.m_isValid = false;
        }
    }

    template<typename T> inline
    Option<T>::Option( T&& t ) noexcept : m_isValid{ true }
    {
        new( &m_data ) T( std::move( t ));
    }

    template<typename T>
    Option<T>::~Option()
    {
        if( m_isValid )
        {
            dataRef().~T();
        }
    }

    template<typename T> inline bool
    Option<T>::isValid() const noexcept
    {
        return m_isValid;
    }

    template<typename T> inline bool
    Option<T>::isDefined() const noexcept
    {
        return m_isValid;
    }

    template<typename T> inline bool
    Option<T>::isEmpty() const noexcept
    {
        return !m_isValid;
    }

    template<typename T>
    Option<T>&
    Option<T>::operator=( Option<T> const& option ) noexcept( std::is_nothrow_copy_constructible_v<T> )
    {
        if( this != &option )
        {
            if( m_isValid && option.m_isValid )
            {
                // both are valid
                dataRef() = option.dataRef();
            }
            else if( m_isValid )
            {
                // this is valid other is not.
                dataRef().~T();
                m_isValid = false;
            }
            else
            {
                // isValid is false, option.isValid is true
                new( &m_data ) T( option.dataRef());
                m_isValid = option.m_isValid;
            }
        }
        return *this;
    }

    template<typename T>
    Option<T>& Option<T>::operator=( Option<T>&& option ) noexcept
    {
        if( m_isValid && option.m_isValid )
        {
            // both are valid
            dataRef() = std::move( option.dataRef() );
        }
        else if( m_isValid )
        {
            // this is valid, other is not.
            dataRef().~T();
            m_isValid = false;
        }
        else
        {
            // this is not valid.
            new( &m_data ) T( std::move( option.dataRef() ));
            m_isValid = option.m_isValid;
        }
        option.m_isValid = false;
        return *this;
    }

    template<typename T>
    T Option<T>::move()
    {
        if( m_isValid )
        {
            T tmp( std::move( dataRef() ));
            dataRef().~T();
            m_isValid = false;
            return tmp;
        }
        else
        {
            throw std::invalid_argument("Option<T>");
        }
    }

    template<typename T>
    T const& Option<T>::get() const
    {
        if( m_isValid )
        {
            return dataRef();
        }
        else
        {
#if defined( __cpp_exceptions )
            throw std::invalid_argument("Option<T>");
#else
            ChefDebug_ASSERT_FAIL();
#endif
        }
    }

    template<typename T> inline Option<T>
    Option<T>::Some( T const& value ) noexcept(noexcept(T(value)))
    {
        return Option<T>{value};
    }

    template<typename T>
    template<typename ... Args>
    Option<T> Option<T>::Some( Args&&... params ) noexcept( noexcept( T(params...) ) )
    {
        return Option<T>( T(std::forward<Args>(params)...) );
    }

    template<typename T> inline Option<T>
    Option<T>::Some( T&& value ) noexcept
    {
        return Option<T>{ std::move(value) };
    }

    template<typename T> inline Option<T>
    Option<T>::None() noexcept
    {
        return Option<T>{};
    }

    template <typename T>
    template<typename F>
    auto Option<T>::map( F f ) const noexcept( noexcept( f( std::declval<T>() ))) -> Option<std::invoke_result_t<F,T>>
    {
        using TargetType = std::invoke_result_t<F, T>;

        if( !isDefined() )
        {
            return Option<TargetType>::None();
        }

        if constexpr( std::is_same_v<TargetType,void> )
        {
            f( dataRef() );
            return Option<void>::Some();
        }
        else
        {
            return Option<TargetType>::Some( f(dataRef()) );
        }
    }

    template<typename T>
    template<typename F>
    auto Option<T>::flatMap( F f ) const noexcept( noexcept( f(std::declval<T>() ))) -> std::invoke_result_t<F,T>
    {
        using TargetType = typename std::invoke_result_t<F, T>::BaseType;

        if( isDefined() )
        {
            return f( dataRef() );
        }
        else
        {
            return Option<TargetType>::None();
        }
    }    

    template<typename T> inline Option<T>
    Some( T const& value )
    {
        return Option<T>::Some( value );
    }

    template<typename T> inline Option<T>
    Some( T&& value )
    {
        return Option<T>::Some( std::forward<T>(value) );
    }

    template<typename T> inline Option<T>
    None()
    {
        return Option<T>::None();
    }

    template<typename T>
    template<typename F>
    Option<T> Option<T>::orElse( F&& f ) const noexcept( noexcept( f() ) )
    {
#if 0
        static_assert(
          std::is_same_v<std::invoke_result_t<F,T>,Option<T>>,
          "Function must return Option<T>"
        );
#endif
        return isDefined() ? *this : f();
    }

    template<typename T> inline T
    Option<T>::getOrElse( T const& defaultValue ) const noexcept
    {
        return isDefined() ? dataRef() : defaultValue;
    }

    template<typename T> inline T const*
    Option<T>::getPtrIfValid() const noexcept
    {
        return isDefined() ?
            reinterpret_cast<T const*>( &m_data ) :
            nullptr;
    }

    template<typename T> inline T*
    Option<T>::getPtrIfValid() noexcept
    {
        return isDefined() ?
               reinterpret_cast<T*>( &m_data ) :
               nullptr;
    }

#if 0
#endif
    template<typename  T> template<typename F>
    void
    Option<T>::foreach( F const& f ) const
#if defined( __cpp_exceptions )
                                           noexcept( std::is_nothrow_invocable_v<F,T> )
#endif
    {
        if( isDefined() )
        {
            f( dataRef() );
        }
    }

    template<typename  T> template<typename F>
    void
    Option<T>::foreach( F const& f )
#if defined( __cpp_exceptions )
                                     noexcept( std::is_nothrow_invocable_v<F,T> )
#endif
    {
        if( isDefined() )
        {
            f( dataRef() );
        }
    }
#if 0
#endif

    template<typename T, typename U> bool
    operator==( Option<T> const& lhs, Option<U> const& rhs ) noexcept
    {
        auto lhsPtr = lhs.getPtrIfValid();
        auto rhsPtr = rhs.getPtrIfValid();

        if( lhsPtr == nullptr && rhsPtr == nullptr )
        {
            return true;
        }
        if( lhsPtr != nullptr && rhsPtr != nullptr )
        {
            return *lhsPtr == *rhsPtr;
        }
        else
        {
            return false;
        }
    }

    template<typename T, typename U> inline bool
    operator!=( Option<T> const& lhs, Option<U> const& rhs ) noexcept
    {
        return !(lhs == rhs);
    }

    template<typename T>
    inline
    Option<T>::OptionIterator::OptionIterator( Option<T> const& option ) noexcept
        : m_item( option.isDefined() ? &option : nullptr )
    {
    }

    template<typename T>
    inline
    Option<T>::OptionIterator::OptionIterator() noexcept
        : m_item( nullptr )
    {
    }

    template<typename T>
    inline typename Option<T>::OptionIterator
    Option<T>::OptionIterator::operator++() noexcept
    {
        m_item = nullptr;
        return *this;
    }

    template<typename T>
    inline T Option<T>::OptionIterator::operator*() const noexcept
    {
        return m_item->get();
    }

    template<typename T>
    inline bool
    Option<T>::OptionIterator::operator==( OptionIterator const& other ) const noexcept
    {
        return m_item == other.m_item;
    }

    template<typename T>
    inline bool
    Option<T>::OptionIterator::operator!=( OptionIterator const& other ) const noexcept
    {
        return m_item != other.m_item;
    }

    template<typename T>
    inline typename Option<T>::OptionIterator Option<T>::begin() const noexcept
    {
        return OptionIterator{ *this };
    }

    template<typename T>
    inline typename Option<T>::OptionIterator Option<T>::end() const noexcept
    {
        return OptionIterator{};
    }

    template<typename T>
    T Option<T>::flatten() const noexcept
    {
        return flatMap( []( T const& o ){ return o; } );
    }

    template<typename T> template<typename F>
    inline Option<std::invoke_result_t<F,T>>
    Option<T>::matchSome( F f ) const noexcept( noexcept( f(std::declval<T>() )))
    {
        return map( f );
    }

    template<typename T> template<typename F>
    std::invoke_result_t<F>
    Option<T>::matchNone( F f ) const noexcept( noexcept( f()))
    {
        return isDefined() ?
            get() :
            f();
    }

    template<typename T> template<typename F>
    Option<T> Option<T>::filter( F f ) const noexcept( noexcept( f(std::declval<T>()) ) )
    {
        if( isDefined() && f( get() ) )
        {
            return *this;
        }
        return None();
    }

    template<typename T, typename ...O>
    ChefFun::Option<T>
    sequence( O... options )
    {
        if( (... || options.isEmpty()) ) {
            return ChefFun::Option<T>::None();
        }
        return ChefFun::Option<T>::Some( T{ options.get()... } );
    }

    template<typename T>
    Option<std::vector<T>>
    sequence( std::vector<Option<T>> const& options )
    {
        std::vector<T> result;
        for( auto const& option : options ){
            if( option.isEmpty() ){
                return Option<std::vector<T>>::None();
            }
            result.push_back( option.get() );
        }
        return Option<std::vector<T>>::Some( result );
    }

}
#endif  /* CHEFFUN_OPTION_HH */


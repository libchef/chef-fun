/**
 * @addtogroup ChefFun
 * @{
 * @file ChefFun/Either.hh
 * @author Massimiliano Pagani
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined( CHEFFUN_EITHER_HH )
#define CHEFFUN_EITHER_HH

#include <ChefFun/Option.hh>
#include <variant>
#include <type_traits>

namespace ChefFun
{
    /**
     * Either is a class that can hold one of two possible values. Values are
     * identified as left or right. If the Either is used to hold the result
     * of an operation that can fail, conventionally the result is hosted in
     * the right part (right as ... right), while if an error occurred it is
     * described by the left part.
     * 
     * Once an Either is constructed you cannot change its value. Moving the
     * value outside the Either is possible, but deprecated.
     *
     * Example n.1 Return an Either from a function
     *
     * @code
     * Either<Error,Value> function( X x, Y y )
     * {
     *     // do something
     *     if( canProduceTheResult )
     *     {
     *         Value result = expression;
     *         return Either<Error,Value>::Right( result );
     *     }
     *     // something went wrong because of error
     *     return Either<Error,Value>::Left( error );
     * }
     * @endcode
     *
     * Example n.2 Using Either with isLeft/isRight
     *
     * @code
     * auto result = function(x,y);
     * if( result.isRight() )
     * {
     *     // successful,
     *     doSomethingWith( result.getRight() );
     * }
     * else
     * {
     *     // failure
     *     log.error( result.getLeft() );
     * }
     * @endcode
     *
     * Example n.3 Using Either with map()
     *
     * @code
     * auto result = function(x,y).map( []( auto&& n ){ return to_string(n); } );
     * // Right of result is of type string, while Left is the same type
     * // returned by function.
     * // If function returns a Left, then result is Left
     * // If function returns a Right and result is Right.
     * @endcode
     *
     * Example n.4 Using Either with for()
     *
     * @code
     * for( n : function(x,y) )
     * {
     *   cout << n << "\n";
     * }
     * @endcode
     *
     * Example n.4 Using Either with pattern matching
     *
     * @code
     * function(x,y)
     *  .matchRight( []( auto&& v ){
     *    doSomethingWith( v );
     *  })
     *  .matchLeft( []( auto&& e ){
     *    log.error( e );
     *  });
     * @endcode
     *
     * Note that matching may also return a value.
     */


    template<typename L, typename R>
    class Either
    {
        public:
            using LeftType = L;
            using RightType = R;

            Either( Either const& ) = default;
            Either( Either&& ) = default;
            Either& operator=( Either const& ) = default;
            Either& operator=( Either&& ) = default;

            /**
             * No public constructor is provided (beside copy and move 
             * constructors). You have to build your Either by calling one of
             * the following static methods.
             * @{
             */
            [[nodiscard]] static Either
            Left( L const& left ) noexcept( std::is_nothrow_copy_constructible_v<L> );
            [[nodiscard]] static Either
            Right( R const& right ) noexcept( std::is_nothrow_copy_constructible_v<R> );
            [[nodiscard]] static Either Left( L&& left ) noexcept;
            [[nodiscard]] static Either Right( R&& right ) noexcept;
            /**
             * @}
             */

            /**
             * Checks the nature of the either.
             * 
             * @retval true if the Either has been constructed by #makeLeft().
             * @retval false if the Either has been constructed by #makeRight().
             * 
             * @see isRight()
             */
            [[nodiscard]] bool isLeft() const noexcept;

            /**
             * Checks the nature of the either.
             * 
             * @retval true if the Either has been constructed by #makeRight().
             * @retval false if the Either has been constructed by #makeLeft().
             * 
             * @see isLeft()
             */
            [[nodiscard]] bool isRight() const noexcept;

            /**
             * Returns the left value.
             * 
             * @retval if the Either has been constructed by #makeRight() then
             *         right value is returned. If the Either is Left, then
             *         exception is thrown.
             * @throws std::bad_variant_access if exceptions are enabled and
             *                                 this is a right Either.
             * 
             * @see getRight()
             */
            [[nodiscard]] L const& getLeft() const;
            [[nodiscard]] L& getLeft();

            /**
             * Returns the right value.
             * 
             * @retval if the Either has been constructed by #makeLeft() then
             *         left value is returned. If the Either is Right, then
             *         exception is thrown.
             * @throws std::bad_variant_access if exceptions are enabled and
             *                                 this is a Left Either.
             *
             * @see getLeft()
             */
            [[nodiscard]] R const& getRight() const;
            [[nodiscard]] R& getRight();

            /**
             * Moves the content of Either if it is right. After this call the
             * option is ready to be destroyed, but shall not be accessed.
             *
             * @return the contained value on the right side.
             * @throws std::bad_variant_access if this is a left either.
             */
            [[deprecated("modifies the Either")]]
            [[nodiscard]] R moveRight() &&;

            /**
             * Swaps this either with another one of the same type. No 
             * exception is thrown in the process.
+             *
             * @param other the other Either to swap with.
             */
            void swap( Either<L,R>& other ) noexcept;

            /**
             * Returns an Either with left and right swapped. This will invoke
             * the copy constructor of the contained value.
             *
             * @return an Either made by swapping left with right.
             */
            Either<R,L> reverse() const;

            /**
             * Maps the either to a new either. Be
             * <ul>
             *   <li> e: Either[L,R]
             *   <li> f: R -> R1
             * </ul>
             * Then e.map(f) yields an Either[L,R1].
             *
             * If e is left, then e.getLeft() == e.map(f).getLeft();
             * If e is right, then f(e.getRight()) == e.map(f).getRight();
             *
             * @tparam F type of the mapping function. Must accepts an R and
             *           return R1.
             * @param f the function to apply of type F
             * @return an Either<L,R1> see the definition above for more
             *         details.
             */
            template<typename F>
            Either<L,std::invoke_result_t<F,R>>
            map( F f ) const noexcept;

            /**
             * Maps the either to a new either, like map, but the function
             * passed as argument is applied only if the Either is left.
             * Be
             * <ul>
             *   <li> e: Either[L,R]
             *   <li> f: L -> L1
             * </ul>
             * Then e.map(f) yields an Either[L1,R].
             *
             * @tparam F type of the mapping function. Must accepts an L. The
             *           return type is used as the new left type for the Either
             *           returned by leftMap.
             * @param f the function to apply of type F
             * @return an Either<L1,R> see the definition above for more
             *         details.
             */
            template<typename F>
            Either<std::invoke_result_t<F,L>,R>
            leftMap( F f ) const noexcept;

            /**
             * Maps the monad while moving the content. This method is handy
             * when you have a resource wrapper that can't be copied, but can
             * be moved.
             *
             * The Either on which mapMove is invoked is depleted of its content
             * and can only be destroyed.
             *
             * @tparam F the transformation function type.
             * @param f the transformation function.
             * @return a new Either obtained by transforming Either.Right via
             *         the transformation function.
             */
            template<typename F>
            Either<L,std::remove_reference_t<std::invoke_result_t<F,R>>>
            mapMove( F f ) && noexcept;

            /**
             * FlatMaps the either to a new either. Be
             * <ul>
             *   <li> e: Either[L,R]
             *   <li> f: R -> Either[L,R1]
             * </ul>
             * Then e.map(f) yields an Either[L,R1].
             *
             * If e is left, then e.getLeft() == e.flatMap(f).getLeft();
             * If e is right, then f(e.getRight()).getRight() == e.flatMap(f).getRight();
             *
             * @tparam F type of the mapping function. Must accepts an R and
             *           return Either<L,R1>.
             * @param f the function to apply of type F
             * @return an Either<L,R1> see the definition above for more
             *         details.
             */
            template<typename F>
            Either<
                LeftType,
                typename std::invoke_result_t<F,R>::RightType
            >
            flatMap( F f ) const noexcept;

            /**
             * Applies a function to the either if it is of Right kind. This is
             * a convenience function and it is equivalent to the following
             * code:
             *
             * @code
             *   if( e.isRight() ) f( e.getRight() )
             * @endcode
             *
             * @param f the function to apply to the right part of the Either
             * @tparam F a function that has one argument of type U.
             * @{
             */
            template<typename F>
            void foreach( F f ) const noexcept( std::is_nothrow_invocable_v<F,R> );

            template<typename F>
            void foreach( F f ) noexcept( std::is_nothrow_invocable_v<F,R> );
            /** @} */

            /**
             * If the Either is right, then the right value is returned, if
             * the Either is left, then elseValue is returned.
             *
             * @param elseValue the value to produce if the Either is left.
             * @return right value or elseValue.
             * @{
             */
            R getOrElse( R&& elseValue ) const noexcept;
            R const& getOrElse( R const& elseValue ) const noexcept;
            /** @} */

            /**
             * If the Either is left, then the left value is returned, if the
             * Either is right, then elseValue is returned.
             *
             * @param elseValue the value to produce if the Either is right.
             * @return left value or elseValue.
             * @{
             */
            L getLeftOrElse( L&& elseValue ) const noexcept;
            L const& getLeftOrElse( L const& elseValue ) const noexcept;
            /**@}*/

            /**
             * Folds the Either so that the a single value of known type is
             * produced.
             *
             * @tparam Fl a function type L -> T
             * @tparam Fr a function type R -> T
             * @param fl a function of type Fl
             * @param fr a function of type Fr
             * @return a value of type T, computed according the following rule.
             * @retval fl(getLeft()) if the either is of type left
             * @retval fr(getRight()) if the either is of type right.
             */
            template <typename Fl, typename Fr>
            std::invoke_result_t<Fr,R> fold( Fl fl, Fr fr ) const noexcept(
                std::is_nothrow_invocable_v<Fl,L> &&
                std::is_nothrow_invocable_v<Fr,R>
            );

            /**
             * Merges the Either when both right and left are the same type.
             *
             * @precondition this method can only invoked on Either<T,T> types.
             *
             * @return if isRight() then getRight(), else if isLeft() then
             *         getLeft().
             */
            R const& merge() const noexcept;

            /**
             * An iterator for the Either. The iterator allows for zero or one
             * iteration on the right part of the either.
             */
            class EitherIterator : public std::input_iterator_tag
            {
                public:
                    explicit EitherIterator( Either<L,R> const& either ) noexcept;
                    EitherIterator() noexcept;
                    EitherIterator operator++() noexcept;
                    R const& operator*() const noexcept;
                    bool operator==( EitherIterator const& other ) const noexcept;
                    bool operator!=( EitherIterator const& other ) const noexcept;
                private:
                    Either<L,R> const* m_item;
            };

            /**
             * Either, once is deemed to be biased, can be considered as a
             * container with zero or one element according to whether the
             * Either is Left or Right.
             *
             * As such it can be used as a container and in C++ all containers
             * have a begin() and end() methods pair to let algorithm use them.
             * 
             * @return an iterator either to the beginnning or to the end of the
             *         Either. For Left Either begin() == end(). For Right
             *         Either *begin() is equivalent to getRight().
             * @{
             */
            [[nodiscard]] EitherIterator begin() const noexcept;
            [[nodiscard]] EitherIterator end() const noexcept;
            ///@}

            /**
             * Converts the Either<L,R> into an Option<R>.
             *
             * @retval Option<R>::Some(getRight()) if isRight()
             * @return Option<R>:None() if isLeft().
             */
            [[nodiscard]] Option<R> toOption() const noexcept( std::is_nothrow_copy_constructible_v<R> );

            /**
             * Pattern matching. By concatenating matchRight and matchLeft you
             * can implement a basic pattern matching structure.
             *
             * @note always matchRight first.
             * @note you can use the result.
             *
             * @tparam F a function type R -> T for matchRight and L -> T for
             *           matchLeft
             * @param f a function of type F.
             * @return the value returned by the function selected by left or
             *         right.
             * @{
             */

            template<typename F>
            [[nodiscard]] Either<L,std::invoke_result_t<F,R>>
            matchRight( F f ) const noexcept( noexcept( f(std::declval<R>()) ));

            template<typename F>
            std::invoke_result_t<F,L>
            matchLeft( F f ) const noexcept( noexcept( f(std::declval<L>() ) ) );
            /** @} */
        private:
            explicit Either( std::variant<L,R>&& implementation );

            std::variant<L,R> m_either;
    };

    template<typename T>
    class MakeEither
    {
        public:
            template<typename R>
            static Either<T,R> right( R&& r )
            {
                return Either<T,R>::Right( r );
            }

            template<typename L>
            static Either<L,T> left( L&& l )
            {
                return Either<L,T>::Left( l );
            }
    };

    /**
     * Checks for Either equality.
     *
     * @tparam L Left type
     * @tparam R Right type
     * @param rhs left hand side term
     * @param lhs right hand side term
     * @retval true if rhs and lhs has same structure and value.
     * @retval false if rhs and lhs does not have the same value.
     */
    template<typename L,typename R>
    bool operator==( Either<L,R> const& rhs, Either<L,R> const& lhs );

    /**
     * Checks for Either inequality.
     *
     * @tparam L Left type
     * @tparam R Right type
     * @param rhs left hand side term
     * @param lhs right hand side term
     * @retval false if rhs and lhs has same structure and value.
     * @retval true if rhs and lhs does not have the same value.
     */
    template<typename L,typename R>
    bool operator!=( Either<L,R> const& rhs, Either<L,R> const& lhs );

#include <ChefFun/Either_L_void.tcc>

// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::: implementation :::
// :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename L,typename R>
    Either<L,R>::Either( std::variant<L,R>&& implementation ) :
                 m_either{ std::move( implementation ) }
    {}

    template<typename L,typename R>
    Either<L,R> Either<L,R>::Left( L const& left ) noexcept( std::is_nothrow_copy_constructible_v<L> )
    {
        return Either{ std::move( std::variant<L,R>{ std::in_place_index<0>, left } ) };
    }

    template<typename L,typename R>
    Either<L,R> Either<L,R>::Left( L&& left ) noexcept
    {
        return Either{ std::move( std::variant<L,R>{ std::in_place_index<0>, std::move(left) }) };
    }

    template<typename L,typename R>
    Either<L,R> Either<L,R>::Right( R const& right ) noexcept( std::is_nothrow_copy_constructible_v<R> )
    {
        return Either{ std::move( std::variant<L,R>{ std::in_place_index<1>, right } )};
    }

    template<typename L,typename R>
    Either<L,R> Either<L,R>::Right( R&& right ) noexcept
    {
        return Either{ std::move( std::variant<L,R>{ std::in_place_index<1>, std::move(right) } )};
    }

    template<typename L,typename R>
    bool Either<L,R>::isLeft() const noexcept
    {
        return m_either.index() == 0;
    }

    template<typename L,typename R>
    bool Either<L,R>::isRight() const noexcept
    {
        return m_either.index() == 1;
    }

    template<typename L,typename R>
    L const&
    Either<L,R>::getLeft() const
    {
        return std::get<0>( m_either );
    }

    template<typename L,typename R>
    R const&
    Either<L,R>::getRight() const
    {
        return std::get<1>( m_either );
    }

    template<typename L,typename R>
    L&
    Either<L,R>::getLeft()
    {
        return std::get<0>( m_either );
    }

    template<typename L,typename R>
    R&
    Either<L,R>::getRight()
    {
        return std::get<1>( m_either );
    }

    template<typename L,typename R>
    inline R Either<L,R>::moveRight() &&
    {
        return std::get<1>( m_either );
    }


    template<typename L,typename R> void
    Either<L,R>::swap( Either<L,R>& other ) noexcept
    {
        m_either.swap( other.m_either );
    }

    template<typename L,typename R>
    Either<R,L> Either<L,R>::reverse() const
    {
        auto const* left = std::get_if<0>( &m_either );
        if( left != nullptr )
        {
            return Either<R,L>::Right( *left );
        }
        auto const* right = std::get_if<1>( &m_either );
        return Either<R,L>::Left( *right );
    }

    template <typename L, typename R>
    R Either<L,R>::getOrElse( R&& elseValue ) const noexcept
    {
        return isRight() ?
            *std::get_if<1>( &m_either ) :
            elseValue;
    }

    template <typename L, typename R>
    R const& Either<L,R>::getOrElse( R const& elseValue ) const noexcept
    {
        return isRight() ?
               *std::get_if<1>( &m_either ) :
               elseValue;
    }

    template <typename L, typename R>
    L Either<L,R>::getLeftOrElse( L&& elseValue ) const noexcept
    {
        return isLeft() ?
               *std::get_if<0>( &m_either ) :
               elseValue;
    }

    template <typename L, typename R>
    L const& Either<L,R>::getLeftOrElse( L const& elseValue ) const noexcept
    {
        return isLeft() ?
               *std::get_if<0>( &m_either ) :
               elseValue;
    }

    template<typename L, typename R>
    template<typename F >
    inline Either<L,std::invoke_result_t<F,R>>
    Either<L,R>::map( F f ) const noexcept
    {
        using TargetType = std::invoke_result_t<F,R>;

        if( isLeft() )
        {
            return Either<L,TargetType>::Left( getLeft() );
        }

        if constexpr( std::is_same_v<TargetType,void> )
        {
            f( *std::get_if<1>( &m_either ) );
            return Either<L,TargetType>::Right();
        }
        else
        {
            return Either<L,TargetType>::Right(f(*std::get_if<1>( &m_either )));
        }
    }

    template<typename L, typename R>
    template<typename F >
    inline Either<std::invoke_result_t<F,L>,R>
    Either<L,R>::leftMap( F f ) const noexcept
    {
        using TargetType = std::invoke_result_t<F,L>;

        if( isRight() )
        {
            return Either<TargetType,R>::Right( getRight() );
        }

        if constexpr( std::is_same_v<TargetType,void> )
        {
            f( *std::get_if<0>( &m_either ) );
            return Either<TargetType,R>::Left();
        }
        else
        {
            return Either<TargetType,R>::Left(f(*std::get_if<0>( &m_either )));
        }
    }

    template<typename L, typename R>
    template<typename F>
    Either<L,std::remove_reference_t<std::invoke_result_t<F,R>>>
    Either<L,R>::mapMove( F f ) && noexcept
    {
        using A = std::remove_reference_t<std::invoke_result_t<F,R>>;
        return isRight() ?
               Either<L,A>::Right( f( std::move(*std::get_if<1>( &m_either ) ))) :
               Either<L,A>::Left( std::move( *std::get_if<0>( &m_either ) ));
    }


    template<typename L, typename R>
    template<typename F>
    Either<
            typename Either<L,R>::LeftType,
            typename std::invoke_result_t<F,R>::RightType
    >
    inline Either<L,R>::flatMap( F f ) const noexcept
    {
        return isRight() ?
            f( *std::get_if<1>( &m_either ) ) :
            //Either<T,typename std::invoke_result_t<F,U>::RightType>::Left( *std::get_if<0>( &m_either ) );
            std::invoke_result_t<F,R>::Left( *std::get_if<0>( &m_either ) );
    }

    template <typename L, typename R>
    template<typename F>
    void Either<L,R>::foreach( F f ) const noexcept( std::is_nothrow_invocable_v<F,R> )
    {
        if( isRight() ) f( getRight() );
    }

    template <typename L, typename R>
    template<typename F>
    void Either<L,R>::foreach( F f ) noexcept( std::is_nothrow_invocable_v<F,R> )
    {
        if( isRight() ) f( getRight() );
    }

    template <typename L, typename R>
    template <typename Fl, typename Fr>
    std::invoke_result_t<Fr,R> Either<L,R>::fold( Fl fl, Fr fr ) const noexcept(
        std::is_nothrow_invocable_v<Fl,L> &&
        std::is_nothrow_invocable_v<Fr,R>
    )
    {
        static_assert( std::is_same_v<std::invoke_result_t<Fr,R>,std::invoke_result_t<Fl,L>> );
        return isLeft() ?
            fl( *std::get_if<0>( &m_either ) ) :
            fr( *std::get_if<1>( &m_either ) );
    }

    template <typename L, typename R>
    R const& Either<L,R>::merge() const noexcept
    {
        return isLeft() ? getLeft() : getRight();
    }

    template<typename L,typename R>
    inline
    Either<L,R>::EitherIterator::EitherIterator( Either<L,R> const& either ) noexcept
        : m_item( either.isRight() ? &either : nullptr )
    {
    }

    template<typename L,typename R>
    inline
    Either<L,R>::EitherIterator::EitherIterator() noexcept
        : m_item( nullptr )
    {
    }

    template<typename L,typename R>
    inline typename Either<L,R>::EitherIterator
    Either<L,R>::EitherIterator::operator++() noexcept
    {
        m_item = nullptr;
        return *this;
    }

    template<typename L,typename R>
    inline R const& Either<L,R>::EitherIterator::operator*() const noexcept
    {
        return m_item->getRight();
    }

    template<typename L,typename R>
    inline bool
    Either<L,R>::EitherIterator::operator==( EitherIterator const& other ) const noexcept
    {
        return m_item == other.m_item;
    }

    template<typename L,typename R>
    inline bool
    Either<L,R>::EitherIterator::operator!=( EitherIterator const& other ) const noexcept
    {
        return m_item != other.m_item;
    }

    template<typename L,typename R>
    inline typename Either<L,R>::EitherIterator Either<L,R>::begin() const noexcept
    {
        return EitherIterator{ *this };
    }

    template<typename L,typename R>
    inline typename Either<L,R>::EitherIterator Either<L,R>::end() const noexcept
    {
        return EitherIterator{};
    }

    template<typename L,typename R>
    inline Option<R> Either<L,R>::toOption() const noexcept ( std::is_nothrow_copy_constructible_v<R> )
    {
        return isRight() ?
            Option<R>::Some( getRight() ) :
            Option<R>::None();
    }

    template<typename L,typename R>
    bool operator==( Either<L,R> const& rhs, Either<L,R> const& lhs )
    {
        return  rhs.isRight() ?
                lhs.isRight() && lhs.getRight() == rhs.getRight() :
                lhs.isLeft() &&  lhs.getLeft() == rhs.getLeft();
    }

    template<typename L,typename R>
    bool operator!=( Either<L,R> const& rhs, Either<L,R> const& lhs )
    {
        return  !(rhs == lhs);
    }

    template<typename L,typename R>
    template<typename F>
    Either<L,std::invoke_result_t<F,R>> Either<L,R>::matchRight( F f ) const noexcept( noexcept( f(std::declval<R>()) ))
    {
        using ReturnType = std::invoke_result_t<F,R>;
        if( isLeft() )
        {
            return Either<L,ReturnType>::Left( getLeft() );
        }
        if constexpr( std::is_same_v<ReturnType,void> )
        {
            f( getRight() );
            return Either<L,ReturnType>::Right();
        }
        else
        {
            return Either<L,ReturnType>::Right( f( getRight() ) );
        }
    }

    template<typename L,typename R>
    template<typename F>
    std::invoke_result_t<F,L>
    Either<L,R>::matchLeft( F f ) const noexcept( noexcept( f( std::declval<L>() ) ) )
    {
        return isRight() ?
            getRight() :
            f( getLeft() );
    }
}

#endif // defined( CHEFFUN_EITHER_HH )
///@}

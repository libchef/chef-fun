/**
 * @file ChefFun/Error.hh
 * @date 2020-03-19
 * @author Massimiliano Pagani
 * @addtogroup ChefFun
 * @{
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined( CHEFFUN_ERROR_HH )
#define CHEFFUN_ERROR_HH

#include <cstdint>
#include <array>
#include <ChefFun/Either.hh>

namespace ChefFun
{
/**
 * Error template is an error code wrapper that enables some functional error
 * handling with the goal of simplifying error management in user code.
 *
 * An underlying type is defined as the error code and can be either an
 * integral value or an enumeration. The only requirement on the error code is
 * that there must be a function named `isError` that distinguish between error
 * codes that represent actual error conditions and error code(s) that
 * represent success.
 *
 * If the success condition code is different from 0, then the user must supply
 * a partial specialization of the ErrorTraits template. The specialization
 * must provide a static function named `isError` that takes an error code
 * and returns true if the error code represents an error condition.
 *
 * Once your function returns an Error instance, you may compose them together
 * using the `andThen` and the `orElse` methods.
 *
 * The `andThen` take a function that is invoked only if the instance on which
 * the method is called is not an error. The function must return an Error
 * instance. The returned instance is the result of the function call.
 *
 * The `orElse` take a function that is invoked only if the instance on which
 * the method is called is an error. The function must return an Error instance.
 * The returned instance is the result of the function call.
 *
 */

    template<typename E>
    class ErrorTraits;

    template<typename E, typename T=ErrorTraits<E>>
    class Error
    {
        private:
            static_assert(
                std::is_nothrow_invocable_r_v<bool, decltype(T::isError), E>,
                "isError(E) not found or may throw"
            );
            static_assert(
                std::is_integral_v<E> || std::is_enum_v<E>,
                "ErrorType must be an integral or enum type"
            );
            using ErrorType = E;
            using BaseErrorType = std::underlying_type_t<ErrorType>;
        public:
            /**
             * Builds an error instance.
             *
             * @param errorCode the error code.
             */
            constexpr explicit Error( ErrorType errorCode ) noexcept;

            /**
             * Returns true if this instance is an error.
             * @retval true this instance is an error.
             * @retval false this instance is not an error.
             */
            [[nodiscard]] bool isError() const noexcept;

            /**
             * Returns true if this instance is not an error.
             * @retval true this instance is not an error.
             * @retval false this instance is an error.
             */
            [[nodiscard]] bool isOk() const noexcept;

            /**
             * Returns the error code.
             * @return the error code.
             */
            [[nodiscard]] ErrorType getCode() const noexcept;

            /**
             * Composes errors. If the instance is not an error, then the
             * provided function is called. The given function must return an
             * Error instance. The returned instance is the result of the
             * function call.
             *
             * If the instance is an error, then the instance itself is
             * returned.
             * 
             * @tparam F the type of the function to call.
             * @param f the function to call.
             * @return the result of the function call or the instance itself.
             */
            template<typename F>
            Error andThen( F f ) const noexcept( std::is_nothrow_invocable_v<F> );

            /**
             * Composes errors. If the instance is an error, then the provided
             * function is called. The given function must return an Error
             * instance. The returned instance is the result of the function
             * call.
             *
             * If the instance is not an error, then the instance itself is
             * returned.
             * 
             * @tparam F the type of the function to call.
             * @param f the function to call.
             * @return the result of the function call or the instance itself.
             */
            template<typename F>
            Error orElse( F f ) const noexcept( std::is_nothrow_invocable_v<F> );

            /**
             * A commodity non error object that can be returned by functions.
             */
            static Error<E,T> const Ok;
        private:
            E m_errorCode;
    };

    /**
     * This free function could be used as an alternative to the `andThen`
     * method. You just pass as many function you need as arguments and the
     * function will call them in sequence until one of them returns an error.
     *
     * @tparam F0 Type of the first function.
     * @tparam E Type of the error
     * @tparam F Types of the other functions to call.
     * @tparam T Trait of the error type.
     * @param f0 the first functon
     * @param f parameter pack of the other functions.
     * @return either the success value or the error returned by the first
     *         function that fails.
     */
    template<typename F0, typename E = std::invoke_result_t<F0>, typename ...F, typename T=ErrorTraits<E>>
    E andThen( F0&& f0, F... f ) noexcept;

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::: inline methods :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename E>
    class ErrorTraits
    {
        public:
            static constexpr bool isError( E error ) noexcept
            {
                using T = std::underlying_type_t<E>;
                return static_cast<T>(error) != 0;
            }

            static constexpr E getSuccess() noexcept
            {
                return (E)0;
            }
    };

    template<typename E, typename T>
    Error<E,T> const Error<E,T>::Ok{ T::getSuccess() };

    template<typename E, typename T>
    inline bool Error<E,T>::isError() const noexcept
    {
        return T::isError(m_errorCode);
    }

    template<typename E, typename T>
    inline bool Error<E,T>::isOk() const noexcept
    {
        return !T::isError(m_errorCode);
    }

    template<typename E, typename T>
    inline E Error<E,T>::getCode() const noexcept
    {
        return m_errorCode;
    }

    template<typename E, typename T>
    constexpr
    Error<E,T>::Error( E errorCode ) noexcept
        : m_errorCode( errorCode )
    {
    }

    template<typename E, typename T>
    template<typename F>
    inline Error<E,T>
    Error<E,T>::andThen( F f ) const noexcept( std::is_nothrow_invocable_v<F> )
    {
        return isOk() ?
            f() :
            *this;
    }

    template<typename E, typename T>
    template<typename F>
    inline Error<E,T>
    Error<E,T>::orElse( F f ) const noexcept( std::is_nothrow_invocable_v<F> )
    {
        static_assert( std::is_invocable_r_v<Error<E,T>, F, Error<E,T>>);
        return isOk() ?
            *this :
            f( *this );
    }

    template<typename E, typename T>
    inline bool
    operator==( Error<E,T> lhs, Error<E,T> rhs ) noexcept
    {
        return lhs.getCode() == rhs.getCode();
    }

    template<typename E, typename T>
    inline bool
    operator!=( Error<E,T> lhs, Error<E,T> rhs ) noexcept
    {
        return !(lhs == rhs);
    }

    template<typename F, typename E = std::invoke_result_t<F>, typename T = ErrorTraits<E>>
    E andThen( F&& f ) noexcept
    {
        return f();
    }

    template<typename F0, typename E, typename ...F, typename T>
    E andThen( F0&& f0, F... f ) noexcept
    {
        E result = f0();
        return result.isError() ?
            result :
            andThen( f... );
    }

}

#endif //CHEFFUN_ERROR_HH

///@}

/**
* @addtogroup ChefFun
* @{
* @file ChefFun/Try.hh
* @author Massimiliano Pagani
*/

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined(CHEFFUN_TRY_HH)
#define CHEFFUN_TRY_HH

#include <ChefFun/Either.hh>
#include <exception>

namespace ChefFun
{
    /**
     * Try is an Either specialization that is used to wrap a function that may
     * throw an exception. The Try object contains either the function result
     * or the thrown exception.
     */

    template<typename T>
    using Try = Either<std::exception_ptr,T>;

    /**
     * TryFn is a helper function that wraps a function that may throw an
     * exception. The function is executed and the result is returned as a Try
     * object.
     *
     * This function throws no exception.
     *
     * @tparam F the type of the function to call. Usually this is automatically
     *           deduced by the compiler.
     * @tparam T the type of the right value of the Try object. Usually this is
     *           automatically deduced by the compiler as the return type of F.
     * @param f the function to call.
     * @return a Try object that contains either the result of the function call
     *         or the thrown exception.
     */

    template<typename F, typename T=std::invoke_result_t<F>, typename R=std::remove_cvref_t<T>>
    Try<R> tryFn( F&& f ) noexcept;

    /**
     * Utility functions to construct a successful or failed Try object.
     *
     * @tparam T the type of the right value of the Try object.
     * @tparam E the type of the exception you want to build the pointer from.
     * @param e the exception pointer
     * @param t the value to wrap in the Try object.
     * @return a Try object that contains either the provided value or the
     *         requested exception.
     * @{
     */
    template<typename T,typename E>
    Try<T> makeFailedTry( E&& e ) noexcept;

    template<typename T>
    Try<T> makeSuccessTry( T&& t ) noexcept;
    /** @} */

    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::: implementation :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename F, typename T, typename R>
    Try<R> tryFn( F&& f ) noexcept
    {
        static_assert( std::is_invocable_v<F> );
        try {
          return Try<R>::Right(f());
        }
        catch( ... )
        {
          return Try<R>::Left(std::current_exception());
        }
    }

    template<typename T,typename E>
    Try<T> makeFailedTry( E&& e ) noexcept
    {
        return Try<T>::Left(std::make_exception_ptr(e));
    }

    template<typename T>
    Try<T> makeSuccessTry( T&& t ) noexcept
    {
        return Try<T>::Right(std::forward<T>(t));
    }

}
#endif // CHEFFUN_TRY_HH

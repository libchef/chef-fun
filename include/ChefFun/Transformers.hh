/**
* @addtogroup ChefFun
* @{
* @file ChefFun/Try.hh
* @author Massimiliano Pagani
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if !defined(CHEFFUN_TRANSFORMERS_HH)
#define CHEFFUN_TRANSFORMERS_HH

#include <ChefFun/Either.hh>
#include <ChefFun/Option.hh>
#include <ChefFun/Error.hh>

namespace ChefFun
{
    /**
     * Transformers are functions that take a value of a type T and return a
     * conversion of such a value into another type.
     *
     * Ideally they have to be considered like extension methods for the type T.
     * This is why the first argument of each transformer is the subject of the
     * transformation and other arguments (if present) are used to complete
     * missing information that may be needed for the transformation.
     *
     * @{
     */

    /**
     * Transforms an Option into an Either. If the option is defined, then the
     * Either will be a Right containing the value of the option. Otherwise the
     * Either will be a Left containing the value passed as second argument.
     *
     * @tparam L Either left type
     * @tparam R Either right type same as Option type
     * @param option the option to transform
     * @param leftValue the value to use for the Left in case the option is not
     *                  defined
     * @return Either<L,R> the result of the transformation.
     * @{
     */

    template<typename L,typename R>
    Either<L,R> toRight( Option<R> const& option, L&& leftValue ) noexcept;

    template<typename L,typename R>
    Either<L,R> toEither( Option<R> const& option, L&& leftValue ) noexcept;
    /** @} */

    /**
     * Transforms an Option into an Either. If the option is defined, then the
     * Either will be a Left containing the value of the option. Otherwise the
     * Either will be a Right containing the value passed as second argument.
     *
     * @tparam L Either left type same as Option type
     * @tparam R Either right type 
     * @param option the option to transform
     * @param rightValue the value to use for the Right in case the option is
     *                   not defined
     * @return Either<L,R> the result of the transformation.
     */

    template<typename L,typename R>
    Either<L,R> toLeft( Option<L> const& option, R&& rightValue ) noexcept;

    /**
     * Transforms an Either into an Option. If the Either is a Right, then the
     * Option will be a Some containing the value of the Either. Otherwise the
     * Option will be a None.
     *
     * @tparam L Either left type
     * @tparam R Either right type same as Option type
     * @param either the either to transform
     * @return Option<R> the result of the transformation.
     */
    template<typename L,typename R>
    Option<R> toOption( Either<L,R> const& either ) noexcept;

    template<typename T,typename E>
    Option<T> toOption( E error, T successValue ) noexcept;

    /**
     * Transforms an Either into an Error, If the Either is Right, then the
     * resulting Error is the success value. If the Either is Left there could
     * be two cases:
     * - Left type is an Error, or
     * - Left type is different type than Error.
     * In the first case the left value is taken and returned. In the latter
     * case, an error value must be provided and the Left value is ignored.
     * 
     * @tparam L the type of the left part of the Either
     * @tparam R the type of the right part of the produced Either
     * @param either the either object to convert
     * @param leftCode the value of the error to use when the Either is Left.
     * @return the Either object produced according to the above rules.
     * @{
     */
    template<typename L,typename R,typename E=L>
    E toError( Either<L,R> const& either ) noexcept;

    template<typename L,typename R,typename E>
    E toError( Either<L,R> const& either, E leftCode ) noexcept;
    /** @} */

    /**
     * Converts an Option to an Error. If the Option is defined, then the 
     * success value of the Error is returned. If the Option is empty, then the
     * provided error code is returned.
     * 
     * @tparam T type of the object in the Option
     * @tparam E type of the Error
     * @param option the option to convert
     * @param code the Error code converting empty options
     * @return an Error derived from `option` according to the described rules
     */
    template<typename T,typename E>
    E toError( Option<T> const& option, E code ) noexcept;

    /** @} */
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::: Implementation :::
    // ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

    template<typename L,typename R>
    Either<L,R> toRight( Option<R> const& option, L&& leftValue ) noexcept
    {
        return option.isDefined() ?
            Either<L,R>::Right( option.get() ) :
            Either<L,R>::Left( std::forward<L>( leftValue ) );
    }

    template<typename L,typename R>
    Either<L,R> toLeft( Option<L> const& option, R&& rightValue ) noexcept
    {
        return option.isDefined() ?
            Either<L,R>::Left( option.get() ) :
            Either<L,R>::Right( std::forward<R>( rightValue ) );
    }

    template<typename L,typename R> inline
    Either<L,R> toEither( Option<R> const& option, L&& leftValue ) noexcept
    {
        return toRight( option, leftValue );
    }

    template<typename L,typename R>
    Option<R> toOption( Either<L,R> const& either ) noexcept
    {
        return either.isRight() ?
            Some<R>( either.getRight() ) :
            None<R>();
    }

    template<typename T,typename E>
    Option<T> toOption( E error, T successValue ) noexcept
    {
        return error.isOk() ?
            Some<T>( successValue ) :
            None<T>();
    }

    template<typename L,typename R,typename E>
    E toError( Either<L,R> const& either ) noexcept
    {
        return either.isRight() ?
            ErrorTraits<E>::getSuccess() :
            either.getLeft();
    }

    template<typename L,typename R,typename E>
    E toError( Either<L,R> const& either, E leftCode ) noexcept
    {
        return either.isRight() ?
            ErrorTraits<E>::getSuccess() :
            leftCode;
    }


    template<typename T,typename E>
    E toError( Option<T> const& option, E code ) noexcept
    {
        return option.isDefined() ?
          ErrorTraits<E>::getSuccess() :
          code;
    }

}

#endif // CHEFFUN_TRANSFORMERS_HH

/** @} */

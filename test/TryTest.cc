/**
* @addtogroup ChefTest
* @{
* @file ChefTest/TryTest.cc
* @author Massimiliano Pagani
*
*/

/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <ChefFun/Try.hh>
#include <string>
#include <gtest/gtest.h>

TEST( ChefFun_Try, constructions )
{
    ASSERT_TRUE( std::is_copy_constructible<ChefFun::Try<int>>::value );
    ASSERT_TRUE( std::is_copy_assignable<ChefFun::Try<int>>::value );
    ASSERT_TRUE( std::is_move_constructible<ChefFun::Try<int>>::value );
    ASSERT_TRUE( std::is_move_assignable<ChefFun::Try<int>>::value );
}

TEST( ChefFun_Try, tryFunctionSuccess )
{
    auto f = []() -> int { return 42; };
    auto t = ChefFun::tryFn( f );
    ASSERT_TRUE( t.isRight() );
    ASSERT_EQ( t.getRight(), 42 );
}

TEST( ChefFun_Try, tryFunctionFailure )
{
    auto f = []() -> int { throw std::runtime_error( "error" ); };
    auto t = ChefFun::tryFn( f );
    ASSERT_TRUE( t.isLeft() );
    try
    {
        std::rethrow_exception( t.getLeft() );
    }
    catch( std::runtime_error& e )
    {
        ASSERT_STREQ( e.what(), "error" );
    }
    catch( ... )
    {
        FAIL();
    }
}

TEST( ChefFun_Try, makeTrySuccess )
{
    auto t = ChefFun::makeSuccessTry( 42 );
    ASSERT_TRUE( t.isRight() );
    ASSERT_EQ( t.getRight(), 42 );
}

TEST( ChefFun_Try, makeTryFailure)
{
    auto t = ChefFun::makeFailedTry<int>( std::runtime_error( "error" ) );
    ASSERT_TRUE( t.isLeft() );
    try
    {
        std::rethrow_exception( t.getLeft() );
    }
    catch( std::runtime_error& e )
    {
        ASSERT_STREQ( e.what(), "error" );
    }
    catch( ... )
    {
        FAIL();
    }
}
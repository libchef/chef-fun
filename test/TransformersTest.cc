/**
* @addtogroup ChefTest
* @{
* @file ChefTest/TransformersTest.cc
* @author Massimiliano Pagani
*
*/

/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <ChefFun/Transformers.hh>
#include <string>
#include <gtest/gtest.h>

TEST( ChefFun_Transformers, fromEitherToOption )
{
    using namespace ChefFun;
    Either<std::string,int> e1 = Either<std::string,int>::Right( 42 );

    auto o1 = toOption( e1 );
    ASSERT_TRUE( o1.isDefined() );
    ASSERT_EQ( o1.get(), 42 );

    Either<std::string,int> e2 = Either<std::string,int>::Left( "error" );
    auto o2 = toOption( e2 );
    ASSERT_FALSE( o2.isDefined() );
}

TEST( ChefFun_Transformers, fromOptionToEitherRight )
{
    using namespace ChefFun;
    Option<int> o1 = Option<int>::Some( 42 );
    auto e1 = toRight( o1, std::string("error") );
    ASSERT_TRUE( e1.isRight() );
    ASSERT_EQ( e1.getRight(), 42 );

    Option<int> o2 = Option<int>::None();
    auto e2 = toRight( o2, std::string("error") );
    ASSERT_TRUE( e2.isLeft() );
    ASSERT_EQ( e2.getLeft(), "error" );
}

TEST( ChefFun_Transformers, fromOptionToEitherLeft )
{
    using namespace ChefFun;
    Option<int> o1 = Option<int>::Some( 42 );
    auto e1 = toLeft( o1, std::string("error") );
    ASSERT_TRUE( e1.isLeft() );
    ASSERT_EQ( e1.getLeft(), 42 );

    Option<int> o2 = Option<int>::None();
    auto e2 = toLeft( o2, 42 );
    ASSERT_TRUE( e2.isRight() );
    ASSERT_EQ( e2.getRight(), 42 );
}

enum class ErrorCode
{
    OK,
    NOT_OK
};

TEST( ChefFun_Transformers, fromOptionToError )
{
    using namespace ChefFun;
    Option<int> o1 = Option<int>::Some( 42 );
    auto e1 = toError( o1, ErrorCode::NOT_OK );
    ASSERT_EQ( e1, ErrorCode::OK );

    Option<int> o2 = Option<int>::None();
    auto e2 = toError( o2, ErrorCode::NOT_OK );
    ASSERT_EQ( e2, ErrorCode::NOT_OK );
}

TEST( ChefFun_Transformers, fromErrorToOption )
{
    using namespace ChefFun;
    auto e1 = Error<ErrorCode>(ErrorCode::OK);
    auto o1 = toOption( e1, 42 );
    ASSERT_TRUE( o1.isDefined() );
    ASSERT_EQ( o1.get(), 42 );

    auto e2 = Error<ErrorCode>( ErrorCode::NOT_OK );
    auto o2 = toOption( e2, 42 );
    ASSERT_TRUE( o2.isEmpty() );
}

TEST( ChefFun_Transformers, fromEitherToError )
{
    using namespace ChefFun;
    Either<std::string,int> e1 = Either<std::string,int>::Right( 42 );
    auto err1 = toError( e1, ErrorCode::NOT_OK );

    ASSERT_EQ( err1, ErrorCode::OK );
    Either<std::string,int> e2 = Either<std::string,int>::Left( "error" );
    auto err2 = toError( e2, ErrorCode::NOT_OK );
    ASSERT_EQ( err2, ErrorCode::NOT_OK );

    Either<ErrorCode,std::string> e3 = Either<ErrorCode,std::string>::Right( "ok" );
    auto err3 = toError( e3 );
    ASSERT_EQ( err3, ErrorCode::OK );

    Either<ErrorCode,std::string> e4 = Either<ErrorCode,std::string>::Left( ErrorCode::NOT_OK );
    auto err4 = toError( e4 );
    ASSERT_EQ( err4, ErrorCode::NOT_OK );
}

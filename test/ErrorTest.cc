/**
 * @addtogroup ChefTest
 * @{
 * @file test/ErrorTest.cc
 * @author Massimiliano Pagani
 * @version 1.0
 * @date 2020-03-19
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFun/Error.hh>
#include <gtest/gtest.h>


using namespace ChefFun;

enum class TheCode
{
    Ok = 0,
    Error1,
    Error2,
    Error3
};

using TheError = Error<TheCode>;

TEST( ChefFun_Error, constructions )
{
    ASSERT_TRUE( std::is_copy_constructible<TheError>::value );
    ASSERT_TRUE( std::is_copy_assignable<TheError>::value );
    ASSERT_TRUE( std::is_move_constructible<TheError>::value );
    ASSERT_TRUE( std::is_move_assignable<TheError>::value );
}

TEST( ChefFun_Error, operator_equal )
{
    constexpr unsigned ErrorA = 0xCA;
    constexpr unsigned ErrorB = 0xDE;

    auto a0 = TheError{ TheCode(ErrorA) };
    auto a1 = TheError{ TheCode(ErrorA) };

    ASSERT_EQ( a0, a0 );
    ASSERT_EQ( a0, a1 );

    auto b0 = TheError{ TheCode(ErrorA) };
    auto b2 = TheError{ TheCode(ErrorB) };

    ASSERT_EQ( a0, b0 );
    ASSERT_NE( a0, b2 );
}

TEST( ChefFun_Error, andThen )
{
    constexpr unsigned Ok = 0;
    constexpr unsigned Fail = 1;

    auto good = TheError{ TheCode(Ok) };
    auto bad = TheError{ TheCode(Fail) };
    ASSERT_EQ( good.andThen( [=](){ return good; }).getCode(), TheCode( Ok ));
    ASSERT_EQ( good.andThen( [=](){ return bad; }).getCode(), TheCode( Fail ));

    ASSERT_EQ( bad.andThen( [=](){ return good; }).getCode(), TheCode( Fail ));
    ASSERT_EQ( bad.andThen( [=](){ return bad; }).getCode(), TheCode( Fail ));
}

TEST( ChefFun_Error, orElse )
{
    constexpr unsigned Ok = 0;
    constexpr unsigned Fail = 1;

    auto good = Error{ TheCode(Ok) };
    auto bad = Error{ TheCode(Fail) };
    ASSERT_EQ( good.orElse( [=](auto&&){ return good; }).getCode(), TheCode(Ok) );
    ASSERT_EQ( good.orElse( [=](auto&&){ return bad; }).getCode(), TheCode(Ok) );

    TheError savedError = TheError::Ok;
    auto r1 = bad.orElse( [&savedError,good](TheError left){
        savedError = left;
        return good;
    }).getCode();
    ASSERT_EQ( r1, TheCode(Ok) );
    ASSERT_EQ( savedError, bad );

    auto r2 = bad.orElse( [&savedError,bad](TheError left){
        savedError = left;
        return bad;
    }).getCode();
    ASSERT_EQ( r2, TheCode(Fail) );
    ASSERT_EQ( savedError, bad );
}

TEST( ChefFun_Error, andThenFreeFunction )
{
    auto good = Error{TheCode::Ok };
    auto bad1 = Error{TheCode::Error1 };
    auto bad2 = Error{TheCode::Error2 };

    auto fnGood = [&good]{ return good; };
    auto fnBad1 = [&bad1]{ return bad1; };
    auto fnBad2 = [&bad2]{ return bad2; };
    ASSERT_EQ( andThen(fnGood), good );
    ASSERT_EQ( andThen(fnBad1), bad1 );

    ASSERT_EQ( andThen(fnGood, fnGood), good );
    ASSERT_EQ( andThen(fnGood, fnBad1), bad1 );
    ASSERT_EQ( andThen(fnBad1, fnGood), bad1 );
    ASSERT_EQ( andThen(fnBad1, fnBad2), bad1 );
}

/**
 * @addtogroup ChefTest
 * @{
 * @file test/EitherTest.cc
 * @author Massimiliano Pagani
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFun/Either.hh>
#include <gtest/gtest.h>

using namespace ChefFun;

TEST( ChefFun_Either, Left_Either )
{
    auto either = Either<int,std::string>::Left( 3 );
    ASSERT_TRUE( either.isLeft() );
    ASSERT_TRUE( !either.isRight() );
    ASSERT_EQ( either.getLeft(), 3 );
    bool exceptionThrown = false;
    try
    {
        (void) either.getRight();
    }
    catch( std::bad_variant_access const& )
    {
        exceptionThrown = true;
    }

    ASSERT_TRUE( exceptionThrown );
}

TEST( ChefFun_Either, Right_Either )
{
    std::string abc{"abc"};

    auto either = Either<int,std::string>::Right( abc );
    ASSERT_TRUE( !either.isLeft() );
    ASSERT_TRUE( either.isRight() );

    bool exceptionThrown = false;
    try
    {
        (void) either.getLeft();
    }
    catch( std::bad_variant_access const& )
    {
        exceptionThrown = true;
    }
    ASSERT_TRUE( exceptionThrown );

    ASSERT_TRUE( either.getRight() == abc );
}

TEST( ChefFun_Either, Reverse )
{
    std::string abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 3 );
    auto eitherR = Either<int,std::string>::Right( abc );
    auto rev1 = eitherL.reverse();
    ASSERT_TRUE( rev1.isRight() );
    ASSERT_EQ( rev1.getRight(), 3 );
    auto rev2 = eitherR.reverse();
    ASSERT_TRUE( rev2.isLeft() );
    ASSERT_EQ( rev2.getLeft(), abc );
}

TEST( ChefFun_Either, getOrElse_getLeftOrElse )
{
    std::string RightValue{"abc" };
    constexpr int LeftValue = 3;
    auto eitherL = Either<int,std::string>::Left( LeftValue );
    auto eitherR = Either<int,std::string>::Right( RightValue );

    ASSERT_EQ( eitherL.getOrElse( "ok"), "ok" );
    ASSERT_EQ( eitherR.getOrElse( "ok" ), RightValue );

    ASSERT_EQ( eitherL.getLeftOrElse( 42 ), LeftValue );
    ASSERT_EQ( eitherR.getLeftOrElse( 42 ), 42 );
}

TEST( ChefFun_Either, functional_map )
{
    std::string const abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto fn = []( std::string const& v ) -> unsigned { return v.size(); };
    auto mapL = eitherL.map( fn );
    auto mapR = eitherR.map( fn );

    ASSERT_TRUE( mapL.isLeft() );
    ASSERT_EQ( mapL.getLeft(), 12 );

    ASSERT_TRUE( mapR.isRight() );
    ASSERT_EQ( mapR.getRight(), abc.size() );
}

TEST( ChefFun_Either, functional_left_map )
{
    std::string const abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto fn = []( int n ) { return n+1; };
    auto mapL = eitherL.leftMap( fn );
    auto mapR = eitherR.leftMap( fn );

    ASSERT_TRUE( mapL.isLeft() );
    ASSERT_EQ( mapL.getLeft(), 12+1 );

    ASSERT_TRUE( mapR.isRight() );
    ASSERT_EQ( mapR.getRight(), abc );
}

TEST( ChefFun_Either, functional_flatMap )
{
    std::string abc{ "abc" };
    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto fn = []( std::string const& v ) -> unsigned { return v.size(); };
    auto mapL = eitherL.map( fn );
    auto mapR = eitherR.map( fn );

    ASSERT_TRUE( mapL.isLeft() );
    ASSERT_EQ( mapL.getLeft(), 12 );

    ASSERT_TRUE( mapR.isRight() );
    ASSERT_EQ( mapR.getRight(), abc.size() );
}

TEST( ChefFun_Either, fold )
{
    std::string abc{ "abc" };

    auto eitherL = Either<int,std::string>::Left( 12 );
    auto eitherR = Either<int,std::string>::Right( abc );

    auto foldL = eitherL.fold(
            []( auto&& x ){ return std::to_string( x ); },
            []( auto&& ){ return std::string{"no"}; }
    );
    ASSERT_EQ( foldL, "12" );

    auto foldR = eitherR.fold(
            [](auto const& ){ return static_cast<size_t>(0); },
            [](auto const& x){ return x.length();}
    );
    ASSERT_EQ( foldR, 3 );

}

TEST( ChefFun_Either, merge )
{
    using Either = Either<int,int>;

    constexpr int RightValue = 2;
    constexpr int LeftValue = 3;

    Either left = Either::Left( LeftValue );
    Either right = Either::Right( RightValue );

    ASSERT_EQ( left.merge(), LeftValue );
    ASSERT_EQ( right.merge(), RightValue );
}

TEST( ChefFun_Either, swap )
{
    using Either = Either<int,std::string>;

    Either oneL = Either::Left( 32 );
    Either twoL = Either::Left( 12 );
    oneL.swap( twoL );
    ASSERT_TRUE( oneL.isLeft() );
    ASSERT_TRUE( twoL.isLeft() );
    ASSERT_EQ( oneL.getLeft(), 12 );
    ASSERT_EQ( twoL.getLeft(), 32 );

    Either oneR = Either::Right( "abc" );
    Either twoR = Either::Right( "def" );
    oneR.swap( twoR );
    ASSERT_TRUE( oneR.isRight() );
    ASSERT_TRUE( twoR.isRight() );
    ASSERT_EQ( oneR.getRight(), "def" );
    ASSERT_EQ( twoR.getRight(), "abc" );

    Either oneM = Either::Right( "abc" );
    Either twoM = Either::Left( 12 );
    oneM.swap( twoM );
    ASSERT_TRUE( oneM.isLeft() );
    ASSERT_TRUE( twoM.isRight() );
    ASSERT_EQ( oneM.getLeft(), 12 );
    ASSERT_EQ( twoM.getRight(), "abc" );
}

TEST( ChefFun_Either, foreach )
{
    using Either = Either<int,std::string>;

    Either left = Either::Left(12);
    Either right = Either::Right("abc");

    int count = 0;
    left.foreach( [&count]( auto const& ){ ++count; });
    ASSERT_EQ( count, 0 );

    right.foreach( [&count](auto const& l){ ASSERT_EQ(l,"abc"); ++count; });
    ASSERT_EQ( count, 1 );
}

TEST( ChefFun_Either, begin_end )
{
    using Either = Either<int,std::string>;

    Either left = Either::Left(12);
    Either right = Either::Right("abc");

    bool isCalled = false;
    for( auto& x: left )
    {
        (void)x;
        isCalled = true;
    }
    ASSERT_FALSE( isCalled );

    for( auto& x: right )
    {
        ASSERT_EQ( x, right.getRight() );
        isCalled = true;
    }
    ASSERT_TRUE( isCalled );
}

TEST( ChefFun_Either, comparison )
{
    using Either = Either<int,std::string>;

    Either left = Either::Left(12);
    Either right = Either::Right("abc");

    ASSERT_NE( left, right );
    ASSERT_EQ( left, left );
    ASSERT_EQ( right, right );
    ASSERT_EQ( left, Either::Left(12));
    ASSERT_NE( left, Either::Left(13));
    ASSERT_EQ( right, Either::Right("abc") );
    ASSERT_NE( right, Either::Right("abcd") );
}

TEST( ChefFun_Either, mapMove )
{
    struct Movable
    {
        Movable()
            : m_moveConstructed{false}
            , m_moveAssigned{false}
        {}

        Movable( Movable&& ) noexcept
            : m_moveConstructed{true}
            , m_moveAssigned{false}
        {}

        Movable& operator=( Movable&& ) noexcept
        {
            m_moveAssigned = true;
            return *this;
        }

        Movable( Movable const& ) = delete;
        Movable& operator=( Movable const& ) = delete;


        bool m_moveConstructed;
        bool m_moveAssigned;
    };

    Either<bool,Movable> source = Either<bool,Movable>::Right( Movable{} );
    auto result = std::move(source).mapMove( []( auto&& x ) { return std::move(x); });
    ASSERT_TRUE( result.isRight() );
    ASSERT_TRUE( result.getRight().m_moveConstructed );
    ASSERT_TRUE( !result.getRight().m_moveAssigned );
}

TEST( ChefFun_Either, Pattern_matching_functional )
{
    using Either = Either<int,std::string>;
    Either a = Either::Right( "abc" );

    int result0 = a
        .matchRight( []( auto&& s ) { return (int)s.size(); } )
        .matchLeft( []( auto&& n ){ return n*3; } );

    ASSERT_EQ( result0, 3 );

    Either b = Either::Left( 10 );
    int result1 = b
        .matchRight( []( auto&& s ) { return (int)s.size(); } )
        .matchLeft( []( auto&& n ){ return n*3; } );

    ASSERT_EQ( result1, 30 );
}

TEST( ChefFun_Either, Pattern_matching_imperative )
{
    using Either = Either<int,std::string>;
    Either a = Either::Right( "abc" );

    int result0{};
    a
        .matchRight( [&result0]( auto&& s ) { result0 = s.size(); } )
        .matchLeft( [&result0]( auto&& n ){ result0 = n*3; } );

    ASSERT_EQ( result0, 3 );

    Either b = Either::Left( 10 );
    int result1{};
    b
        .matchRight( [&result1]( auto&& s ) { result1 = s.size(); } )
        .matchLeft( [&result1]( auto&& n ){ result1 = n*3; } );

    ASSERT_EQ( result1, 30 );
}

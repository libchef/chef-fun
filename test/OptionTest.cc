/**
 * @addtogroup ChefTest
 * @{
 * @file test/OptionTest.cc
 * @author Massimiliano Pagani
 *
 */

/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ChefFun/Option.hh>
#include <string>
#include <gtest/gtest.h>

//NOLINTBEGIN "readability-identifier-length"

using namespace ChefFun;

TEST( ChefFun_Option,  copy_constructors )
{
    auto a = Option<int>::Some(3);
    Option<int> b{a};

    ASSERT_TRUE( b.isDefined() );
    ASSERT_EQ( b.get(), 3 );

    auto c = Option<int>::None();
    Option<int> d{c};
    ASSERT_TRUE( c.isEmpty() );
}

TEST( ChefFun_Option,  move_constructors )
{
    auto a = Option<int>::Some(3);
    Option<int> b( std::move(a) );

    ASSERT_TRUE( b.isDefined() );
    ASSERT_EQ( b.get(), 3 );

    ASSERT_TRUE( a.isEmpty() );
}

TEST( ChefFun_Option,  base_test )
{
    using OptionInt = Option<int>;

    OptionInt a = None<int>();
    ASSERT_TRUE( a.isEmpty() );

    bool caught=false;
    try
    {
        int x = a.move();
        (void)x;
        assert( false );
    }
    catch( ... )
    {
        caught = true;
    }
    assert( caught );

    a = Some(3);
    ASSERT_TRUE( a.isDefined() );
    OptionInt b{ OptionInt::Some(3)};
    ASSERT_TRUE( b.isDefined() );
    ASSERT_EQ( None<int>().getOrElse(3), 3 );
    ASSERT_EQ( OptionInt::Some(42).getOrElse(3), 42 );

    int c = 1+b.get();
    ASSERT_EQ( c, 1+3 );
}

TEST( ChefFun_Option,  orElse )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    ASSERT_TRUE( a.orElse( [](){ return Option<int>::Some(4); } ).isDefined() );
    ASSERT_TRUE( a.orElse( [](){ return Option<int>::Some(4); } ).get() == 3 );

    ASSERT_TRUE( b.orElse( [](){ return Option<int>::Some(4); } ).isDefined() );
    ASSERT_TRUE( b.orElse( [](){ return Option<int>::Some(4); } ).get() == 4 );
}

static int f( std::string_view x )
{
    return x.length();
}

TEST( ChefFun_Option,  vararg_constructor )
{
    class Test
    {
        public:
            Test( int x, int y ) : m_x{x},m_y{y} {}
            int m_x;
            int m_y;
    };

    auto o = Option<Test>::Some( 4, 2 );
    ASSERT_TRUE( o.isDefined() );
    ASSERT_TRUE( o.get().m_x == 4 );
    ASSERT_TRUE( o.get().m_y == 2 );
}

TEST( ChefFun_Option,  map_test )
{
    Option<std::string> test = Option<std::string>::Some( std::string{"abc"});
    Option<int> result = test.map( f );
    ASSERT_TRUE( result.isDefined() );
    ASSERT_EQ( result.get(), 3 );
}

TEST( ChefFun_Option,  assignment )
{
    Option<int> a{ Option<int>::Some(3) };
    Option<int> b{ Option<int>::Some(2) };

    a = b;
    ASSERT_TRUE( a.isDefined() );
    ASSERT_TRUE( b.isDefined() );
    ASSERT_TRUE( a.get() == 2 );
    ASSERT_TRUE( b.get() == 2 );
    ASSERT_TRUE( a == b );

    Option<int> c{ Option<int>::None() };
    a = c;
    ASSERT_TRUE( a.isEmpty() );
    ASSERT_TRUE( a == c );

    a = b;
    ASSERT_TRUE( a.isDefined() );
    ASSERT_TRUE( b.isDefined() );
    ASSERT_TRUE( a.get() == 2 );
    ASSERT_TRUE( b.get() == 2 );
    ASSERT_TRUE( a == b );

    a = c;
    // both a and c are None
    c = a;
    ASSERT_TRUE( a.isEmpty() );
    ASSERT_TRUE( c.isEmpty() );

}

TEST( ChefFun_Option,  foreach )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    bool isCalled = false;
    a.foreach( [&isCalled]( int x ){ isCalled = true; });
    ASSERT_TRUE( isCalled );

    isCalled = false;
    b.foreach( [&isCalled]( int x ){ isCalled = true; });
    ASSERT_TRUE( !isCalled );
}

TEST( ChefFun_Option,  begin_end )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    bool isExecuted = false;
    for( auto x : a )
    {
        isExecuted = true;
        ASSERT_TRUE( x == 3 );
    }
    ASSERT_TRUE( isExecuted );

    isExecuted = false;
    for( auto x : b )
    {
        (void)x;
        isExecuted = true;
    }
    ASSERT_TRUE( !isExecuted );
}

TEST( ChefFun_Option,  flatten )
{
    auto a = Option<Option<int>>::Some(Option<int>::Some(3));
    auto b = Option<Option<int>>::Some(Option<int>::None());
    auto c = Option<Option<int>>::None();

    ASSERT_TRUE( a.flatten() == Option<int>::Some(3) );
    ASSERT_TRUE( b.flatten() == Option<int>::None() );
    ASSERT_TRUE( c.flatten() == Option<int>::None() );
}

TEST( ChefFun_Option,  pattern_matching_functional )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    int result0 = a
        .matchSome( []( auto&& n ){ return n*2; } )
        .matchNone( [](){ return 0; } );

    ASSERT_EQ( result0, 6 );

    int result1 = b
        .matchSome( []( auto&& n ){ return n*2; } )
        .matchNone( [](){ return 0; } );

    ASSERT_EQ( result1, 0 );
}

TEST( ChefFun_Option,  pattern_matching_imperative )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    int result{};

    a
        .matchSome( [&result]( auto&& n ){ result = n*2; } )
        .matchNone( [&result](){ result = 0; } );

    ASSERT_EQ( result, 6 );

    b
        .matchSome( [&result]( auto&& n ){ result = n*2; } )
        .matchNone( [&result](){ result = 0; } );

    ASSERT_EQ( result, 0 );
}

TEST( ChefFun_Option, filter )
{
    auto a = Option<int>::Some(3);
    auto b = Option<int>::None();

    auto fa = a.filter( []( int n ){ return n == 3; } );
    ASSERT_TRUE( fa.isDefined() );
    ASSERT_EQ( fa, a );

    ASSERT_TRUE( a.filter( []( auto&& n ){ return n == 4; } ).isEmpty() );
    ASSERT_TRUE( b.filter( []( auto&& n ){ return n == 3; } ).isEmpty() );
}

TEST( ChefFun_Option,  sequence_vector )
{
    std::vector<Option<int>> v;
    v.push_back( Option<int>::Some(1) );
    v.push_back( Option<int>::Some(2) );
    v.push_back( Option<int>::Some(3) );

    auto result = sequence( v );

    ASSERT_TRUE( result.isDefined() );
    auto const& r = result.get();
    ASSERT_EQ( r.size(), 3 );
    ASSERT_EQ( r[0], 1 );
    ASSERT_EQ( r[1], 2 );
    ASSERT_EQ( r[2], 3 );

    v[0] = Option<int>::None();
    ASSERT_TRUE( sequence( v ).isEmpty() );

    v[0] = Option<int>::Some(1);
    v[1] = Option<int>::None();
    ASSERT_TRUE( sequence( v ).isEmpty() );

    v[1] = Option<int>::Some(2);
    v[2] = Option<int>::None();
    ASSERT_TRUE( sequence( v ).isEmpty() );
}

TEST( ChefFun_Option,  sequence_tuple )
{
    struct Test
    {
        int a;
        std::string b;
        float c;
    };

    auto result = sequence<Test>(
        Option<int>::Some(1),
        Option<std::string>::Some("abc"),
        Option<float>::Some(3.14f)
    );

    ASSERT_TRUE( result.isDefined() );
    auto const& r = result.get();
    ASSERT_EQ( r.a, 1 );
    ASSERT_EQ( r.b, "abc" );
    ASSERT_EQ( r.c, 3.14f );

    ASSERT_TRUE( sequence<Test>(
        Option<int>::None(),
        Option<std::string>::Some("abc"),
        Option<float>::Some(3.14f)
    ).isEmpty() );

    ASSERT_TRUE( sequence<Test>(
        Option<int>::Some(1),
        Option<std::string>::None(),
        Option<float>::Some(3.14f)
    ).isEmpty() );

    ASSERT_TRUE( sequence<Test>(
        Option<int>::Some(1),
        Option<std::string>::Some("abc"),
        Option<float>::None()
    ).isEmpty() );
}

//NOLINTEND

///@}

# ChangeLog

More recent versions at the top.

## 1.3.0 - 2023-10-03
* Add Try
* Add Option::orElse
* Add Option::sequence
* Add Error::andThen

## 1.2.0 - 2023-03-05

* Add sequence and traverse methods to Option and Either
* Add filter method to Option and Either

## 1.1.0 - 2023-03-05

Monadic error code handling added.

## 1.0.1 - 2023-02-13

cmake version requirement reduced

## 1.0.0 - 2023-02-01

Initial release
